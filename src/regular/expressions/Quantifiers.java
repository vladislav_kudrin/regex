package regular.expressions;

import java.util.regex.Pattern;

/**
 * Shows how quantifiers work.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/04/2020
 */
public class Quantifiers {
    public static void main(String[] args) {
        System.out.println(match("10{2}"));

        System.out.println(match("10{1,3}"));

        System.out.println(match("10{2,}"));

        System.out.println(match("10?"));

        System.out.println(match("10*"));

        System.out.println(match("10+"));
    }

    /**
     * Matches {@code examplePattern} with {@code index} and saves a result to {@code result}.
     *
     * @param example a singular expression.
     * @return a matches result.
     */
    private static String match(String example) {
        StringBuilder result = new StringBuilder();
        Pattern examplePattern = Pattern.compile(example);

        for(int index = 1; index <= 1000; index *= 10) {
            result.append(examplePattern.matcher(Integer.toString(index)).find()).append(" ");
        }

        return result.append("\n").toString();
    }
}