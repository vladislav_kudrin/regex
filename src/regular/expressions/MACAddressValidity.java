package regular.expressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Checks MAC-address for validity.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/04/2020
 */
public class MACAddressValidity {
    public static void main(String[] args) {
        String example = "77:a3:d2:01:ff:63";
        int counter = 0;
        Pattern examplePattern = Pattern.compile("^((\\p{XDigit}{2}([:-]|$)){6})$");
        Matcher exampleMatcher = examplePattern.matcher(example);

        while(exampleMatcher.find()) {
            counter++;

            System.out.println("Match found '" +
                    example.substring(exampleMatcher.start(), exampleMatcher.end()) +
                    "'. Starting at index " + exampleMatcher.start() +
                    " and ending at index " + exampleMatcher.end() + '.');
        }

        System.out.println("Matches found: " + counter + '.');
    }
}