package regular.expressions;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Checks IPv4 for validity.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/04/2020
 */
public class IPv4Validity {
    public static void main(String[] args) {
        String example = "0.0.0.0";
        int counter = 0;
        Pattern examplePattern = Pattern.compile("^(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))$");
        Matcher exampleMatcher = examplePattern.matcher(example);

        while(exampleMatcher.find()) {
            counter++;

            System.out.println("Match found '" +
                    example.substring(exampleMatcher.start(), exampleMatcher.end()) +
                    "'. Starting at index " + exampleMatcher.start() +
                    " and ending at index " + exampleMatcher.end() + '.');
        }

        System.out.println("Matches found: " + counter + '.');
    }
}