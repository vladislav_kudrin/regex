package regular.expressions;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Checks email for a validity.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/04/2020
 */
public class EMailValidity {
    public static void main(String[] args) {
        String example = "example@gmail.com";
        int counter = 0;
        Pattern examplePattern = Pattern.compile("^((\\w|[-+])+(\\.[\\w-]+)*@[\\w-]+((\\.[\\d\\p{Alpha}]+)*(\\.\\p{Alpha}{2,})*)*)$");
        Matcher exampleMatcher = examplePattern.matcher(example);

        while(exampleMatcher.find()) {
            counter++;

            System.out.println("Match found '" +
                    example.substring(exampleMatcher.start(), exampleMatcher.end()) +
                    "'. Starting at index " + exampleMatcher.start() +
                    " and ending at index " + exampleMatcher.end() + '.');
        }

        System.out.println("Matches found: " + counter + '.');
    }
}