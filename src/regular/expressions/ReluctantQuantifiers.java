package regular.expressions;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Shows how reluctant quantifiers work.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/04/2020
 */
public class ReluctantQuantifiers {
    public static void main(String[] args) {
        String example = "196.198.1.197";
        int counter = 0;
        Pattern examplePattern = Pattern.compile(".*?19");
        Matcher exampleMatcher = examplePattern.matcher(example);

        while(exampleMatcher.find()) {
            counter++;

            System.out.println("Match found '" +
                    example.substring(exampleMatcher.start(), exampleMatcher.end()) +
                    "'. Starting at index " + exampleMatcher.start() +
                    " and ending at index " + exampleMatcher.end());
        }

        System.out.println("\n" + "Matches found: " + counter);
    }
}