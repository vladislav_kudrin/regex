package regular.expressions;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Checks IPv6 for validity.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/04/2020
 */
public class IPv6Validity {
    public static void main(String[] args) {
        String example = "1:1:1:1:1:1:1:1";
        int counter = 0;
        Pattern examplePattern = Pattern.compile("^(" +
                "(\\p{XDigit}{1,4}:){7}\\p{XDigit}{1,4}|" +
                "(\\p{XDigit}{1,4}:{1,7}:|" +
                "(\\p{XDigit}{1,4}:){1,6}(:(\\p{XDigit}{1,4}))|" +
                "(\\p{XDigit}{1,4}:){1,5}(:(\\p{XDigit}{1,4})){1,2}|" +
                "(\\p{XDigit}{1,4}:){1,4}(:\\p{XDigit}{1,4})){1,3}|" +
                "(\\p{XDigit}{1,4}:){1,3}(:(\\p{XDigit}{1,4})){1,4}|" +
                "(\\p{XDigit}{1,4}:){1,2}(:(\\p{XDigit}{1,4})){1,5}|" +
                "(\\p{XDigit}{1,4}:)(:(\\p{XDigit}{1,4})){1,6}|" +
                "(:((:(\\p{XDigit}{1,4})){1,7})|:)|" +
                "::(ffff(:0{1,4})?:)?((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])|" +
                "((\\p{XDigit}{1,4}):){1,4}:((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])|" +
                ")$");
        Matcher exampleMatcher = examplePattern.matcher(example);

        while(exampleMatcher.find()) {
            counter++;

            System.out.println("Match found '" +
                    example.substring(exampleMatcher.start(), exampleMatcher.end()) +
                    "'. Starting at index " + exampleMatcher.start() +
                    " and ending at index " + exampleMatcher.end() + '.');
        }

        System.out.println("Matches found: " + counter + '.');
    }
}