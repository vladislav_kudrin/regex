package regular.expressions;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Checks a phone number for validity.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/04/2020
 */
public class PhoneNumberValidity {
    public static void main(String[] args) {
        String example = "+380442283228";
        int counter = 0;
        Pattern examplePattern = Pattern.compile("^((\\+380)([0-9]{9}))$");
        Matcher exampleMatcher = examplePattern.matcher(example);

        if(exampleMatcher.matches()) {
            System.out.println("Phone number '" + example + "' is correct.");
        }
        else {
            System.out.println("Phone number '" + example + "' is incorrect.");
        }
    }
}