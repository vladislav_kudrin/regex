package regular.expressions;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Shows how capturing group works.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/03/2020
 */
public class Grouping {
    public static void main(String[] args) {
        System.out.print(match("(\\d+)", 0));
        System.out.println(match("(\\d+)", 1));

        System.out.print(match("(\\d+).*\\1", 0));
        System.out.println(match("(\\d+).*\\1", 1));

        System.out.println(match("(?:Mouse|Keyboard)Listener"));

        System.out.println(match("Java (?=[78])"));

        System.out.println(match("Java (?![78])"));
    }

    /**
     * Matches {@code examplePattern} with {@code exampleMatcher} and saves a result to {@code result}.
     *
     * @param example a regular expression.
     * @param group a group's number.
     * @return a matches result.
     */
    private static String match(String example, int group) {
        StringBuilder result = new StringBuilder();
        Pattern examplePattern = Pattern.compile(example);
        Matcher exampleMatcher;

        exampleMatcher = examplePattern.matcher("2016 year. 2017 year. 2015 year.");

        while(exampleMatcher.find()) result.append(exampleMatcher.group(group)).append(" ");

        return result.append("\n").toString();
    }

    /**
     * Matches {@code examplePattern} with {@code exampleMatcher} and saves a result to {@code result}.
     *
     * @param example a regular expression.
     * @return a matches result.
     */
    private static String match(String example) {
        StringBuilder result = new StringBuilder();
        Pattern examplePattern = Pattern.compile(example);
        Matcher exampleMatcher;

        exampleMatcher = examplePattern.matcher("MouseListener KeyboardListener Java 7 Java 8 Java 9");

        while(exampleMatcher.find()) result.append(exampleMatcher.group()).append(" ");

        return result.append("\n").toString();
    }
}