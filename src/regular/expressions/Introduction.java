package regular.expressions;

import java.util.regex.Pattern;

/**
 * Shows how regular expressions work.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/03/2020
 */
public class Introduction {
    public static void main(String[] args) {
        System.out.println(match("[a-z]+"));

        System.out.println(match("[a-zA-Z0-9]"));
    }

    /**
     * Matches {@code examplePattern} with {@code exampleMatcher} and saves a result to {@code result}.
     *
     * @param example a regular expression.
     * @return a matches result.
     */
    private static String match(String example) {
        Pattern examplePattern = Pattern.compile(example);
        String result = "";

        result = result + examplePattern.matcher("a b c d 1 2 3 4").find() + "\n";
        result = result + examplePattern.matcher("A B C D 1 2 3 4").find() + "\n";
        return result + examplePattern.matcher("A B C D a b c d 1 2 3 4").find() + "\n";
    }
}