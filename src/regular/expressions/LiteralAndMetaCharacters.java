package regular.expressions;

import java.util.regex.Pattern;

/**
 * Shows how Literals and metacharacters work.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/03/2020
 */
public class LiteralAndMetaCharacters {
    public static void main(String[] args) {
        System.out.println(match("^[a-e]"));

        System.out.println(match("[a-e]$"));

        System.out.println(match(".[0-9]"));

        System.out.println(match(".[0-9]|.[a-c]"));

        System.out.println(match("[^0-9]"));

        System.out.println(match("\\d"));

        System.out.println(match("\\D"));

        System.out.println(match("\\s"));

        System.out.println(match("\\S"));
    }

    /**
     * Matches {@code examplePattern} with {@code exampleMatcher} and saves a result to {@code result}.
     *
     * @param example a regular expression.
     * @return a matches result.
     */
    private static String match(String example) {
        Pattern examplePattern = Pattern.compile(example);
        String result = "";

        result = result + examplePattern.matcher("a b c d e f g h").find() + "\n";
        result = result + examplePattern.matcher("f g h a b c").find() + "\n";
        return result + examplePattern.matcher("1 2 3").find() + "\n";
    }
}